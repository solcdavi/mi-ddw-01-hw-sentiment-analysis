require('newrelic');

var koa = require('koa'),
    responseTime = require('koa-response-time'),
    session = require('koa-session'),
    views = require('koa-views'),
    json = require('koa-json'),
    bodyParser = require('koa-body-parser'),
    serve = require('koa-static'),
    router = require('koa-router'),
    mongo = require('mongodb'),
    Q = require('q'),
    sentiment = require('sentiment'),
    fb = require('fb');


// =================================================================================
// ### KOA CONFIG
// =================================================================================


var app = koa();

app.keys = ['secrets'];

var listenPort = process.env.PORT || 3000;


// =================================================================================
// ### KOA MIDDLEWARE
// =================================================================================


// x-response-time
app.use(responseTime());

// session
app.use(session());

// static
app.use(serve(__dirname + '/static'));

// body parser
app.use(bodyParser());

// json
app.use(json());

// views
app.use(views('html', {
    html: 'underscore'
}));

// router
app.use(router(app));


// =================================================================================
// ### ROUTES
// =================================================================================


app.post('/api/sentiment', function *(next) {

    var req = this.request.body;

    this.type = 'application/json';
    var that = this;
    sentiment(req.message, function (err, ans) {
        that.body = ans;
    });;

});


app.post('/api/facebook', function *(next) {

    var req = this.request.body;
    var fbResponse = null;

    var that = this;

    var senti = function(fbRes, i) {
        var j = i;
        sentiment(fbRes.data[j].message, function (err, ans) {
            fbRes.data[j].sentiment = ans;
            if (j == 0) {
                console.log("sending");
            };
        });
    }

    function delay(milliseconds) {
        var deferred = Q.defer();
        setTimeout(deferred.resolve, milliseconds);
        return deferred.promise;
    }

    fb.api('' + req.postId + "/comments", function (fbRes) {
        if(!fbRes || fbRes.error) {
            console.log(!fbRes ? 'error occurred' : fbRes.error);
            that.status = 500;
            return;
        }
        fbResponse = fbRes;
        for (var i = fbRes.data.length - 1; i >= 0; i--) {
            senti(fbRes, i);
        };
    });

    yield delay(1000);

    this.type = 'application/json';
    this.body = fbResponse;
    console.log("sending...");
});


app.get('/', function *(next) {

    this.locals = {
        session: this.session
    };

    yield this.render('index');
});


app.get('/facebook', function *(next) {

    yield this.render('facebook');

});


// =================================================================================
// ### START SERVER
// =================================================================================


app.listen(listenPort);
console.log("Running on port: " + listenPort);
