Sentiment analysis tool: [sentiment](https://github.com/thisandagain/sentiment "sentiment")

Running app on Heroku: [http://solcdavi-middw-01.herokuapp.com](http://solcdavi-middw-01.herokuapp.com/ "MI-DDW / solcdavi / 01.HW")

Running localy:

nodejs 0.11.12

npm install
node -- harmony index.js

Go to: [localhost:3000](http://localhost:3000 "localhost")
