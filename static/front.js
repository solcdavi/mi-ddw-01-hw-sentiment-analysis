function DDWCtrl($scope) {
  $scope.message = "";
  $scope.response = null;

  $scope.submit = function() {
        $.post(
            'api/sentiment',
            {
                message: $scope.message
            }
        ).done(function( answer ) {
            $scope.$apply(function () {
                $scope.response = answer;
            });
        });
  };
}

function FBCtrl($scope) {
  $scope.postId = null;
  $scope.response = null;

  $scope.submit = function() {
        $.post(
            'api/facebook',
            {
                postId: $scope.postId
            }
        ).done(function( answer ) {
            $scope.$apply(function () {
                $scope.response = answer;
            });
        });
  };
}
